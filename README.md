# iot-bee-talk

IoT-Bee-Talk: improve honeybee health


## Basic setup

```bash
$ make install
$ make run
```

## Dependancies
First, you need some Python packages (available through `make install`):

- sounddevice